(defproject async-server "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-jetty-adapter "1.3.2"]
                 [compojure "1.3.4"]
                 [alaisi/postgres.async "0.5.0"]
                 [cheshire "5.5.0"]
                 [http-kit "2.1.8"]]
  :jvm-opts ["-Xmx1g" "-server"] 
  :main ^:skip-aot async-server.server
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
