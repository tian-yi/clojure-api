(ns async-server.server
  (:gen-class)
  (:require [postgres.async :refer :all]
            [cheshire.core :as json :refer [generate-string]]
            [clojure.core.async :refer [<!!]]
            [compojure.handler :refer [site]] ; form, query params decode; cookie; session, etc
            [compojure.core :refer [defroutes GET POST DELETE ANY context]]
            [org.httpkit.server :refer [run-server]]))


(def db (open-db {:hostname "localhost"
                  :port 5432
                  :database "clojure_api"
                  :username ""
                  :password ""
                  :pool-size 25})) 

(defn ty-query []
  (<!! (execute! db ["select * from things"])))


(defn json-response [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/json"}
   :body (json/generate-string data)})

(defroutes ty-routes
  (GET "/api/things/" []
       (json-response (ty-query))))

(defn -main
  [& args]
  (run-server (site #'ty-routes) {:port 9001 :threads 25}))
